
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author watan
 */
public class CreateTableUser {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        //Connection
        try {
            //Connection
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = conn.createStatement();
            String sql = "CREATE TABLE USER"
                    + "(ID INT PRIMARY KEY NOT NULL,"
                    + "USERNAME CHAR(50) NOT NULL,"
                    + "PASSWORD CHAR(50) NOT NULL)";
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
            System.exit(0);
        }

    }
}
